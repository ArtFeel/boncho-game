--====================================================================--
-- ROCKET SPACE
--====================================================================--

------------------------------
-- Imports
------------------------------

local CSL = require( "libs.crawlspaceLib" )

------------------------------
-- Init Configuration
------------------------------

display.setStatusBar( display.HiddenStatusBar )

------------------------------
-- Debug
------------------------------
if true then
	print( " === DEBUG MODE ON === " )

	-- set console output
	io.output():setvbuf( "no" )

	-- load the Remote
	-- remote = require( "lib.remote" )
	-- start the remote on port 8080
	-- remote.startServer( "8080" )

	-- enable on device debugging
	-- Enable( "debug" )
end

------------------------------
-- Load Settings
------------------------------
-- SettingsManager.load()
-- print(settings.playerInfo.statistic)

------------------------------
-- Begin
------------------------------

-- local mainScreen = MainScreen.new()
-- mainScreen:start()



----- CREATE GAME BASE -----
local myMap = {}
local mapWidth = 5
local mapHeight = 5

local gameGroup = display.newGroup()
local eyesGroup = display.newGroup()
local background = display.newImageRect( "assets/images/background.png", 320, 480 )

local selectedItem = nil

gameGroup:insert( background, eyesGroup )
background:center()
eyesGroup:setPos( 10, 120 )


----- COLORIZE FUNCTION -----
local function clean()
	for y = 1, mapHeight do
		for x = 1, mapWidth do
			local cell = myMap[y][x]
			if cell then
				if cell.state ~= "hidden" then
					cell.alpha = 1
					cell:setFillColor( 255, 255, 255 )
					cell.state = "normal"
				else
					cell.alpha = 0.2
					cell:setFillColor( 255, 255, 255 )
				end
				cell.groupId = nil
			end
		end
	end

	selectedItem = nil
end

local function colorize( tX, tY )
	local x, y = tX, tY
	myMap[y][x].alpha = 0.5
	myMap[y][x]:setFillColor( 128, 128, 128 )

	-- vertical
	local vBounds = { 2, 1, 1, 1, 2, 4, 4, 5, 4, 4 }
	local vY1, vY2 = y - 1, y + 1
	local vX1, vX2 = x, x
	if vY1 < vBounds[x] then vY1 = vBounds[x + 5] end
	if vY2 > vBounds[x + 5] then vY2 = vBounds[x] end

	local vCell1 = myMap[vY1][x]
	if vCell1 then
		vCell1:setFillColor( 255, 0, 0 )
		-- vCell1.state = "enabled"
		vCell1.groupId = 1
	end

	local vCell2 = myMap[vY2][x]
	if vCell2 then
		vCell2:setFillColor( 255, 0, 0 )
		-- vCell2.state = "enabled"
		vCell2.groupId = 1
	end

	-- horizontal top-left — bottom-right
	local hBounds = { 
		{4,1}, {3,1}, {2,1}, {1,2}, {1,3},
		{5,3}, {4,4}, {4,5}, {3,5}, {2,5},
	}
	-- on normal cells
	if x % 2 == 0 then
		vY1, vY2 = y, y + 1
	else
		vY1, vY2 = y - 1, y
	end
	vX1, vX2 = x - 1, x + 1
	-- on bounds cells
	for i = 1, mapWidth do
		if y == hBounds[i][1] and 
			x == hBounds[i][2] then
			vY1 = hBounds[i+5][1]
			vX1 = hBounds[i+5][2]
		end
	end
	for i = 6, mapWidth * 2 do
		if y == hBounds[i][1] and 
			x == hBounds[i][2] then
			vY2 = hBounds[i-5][1]
			vX2 = hBounds[i-5][2]
		end
	end

	vCell1 = myMap[vY1][vX1]
	if vCell1 then
		vCell1:setFillColor( 0, 255, 0 )
		-- vCell1.state = "enabled"
		vCell1.groupId = 2
	end

	vCell2 = myMap[vY2][vX2]
	if vCell2 then
		vCell2:setFillColor( 0, 255, 0 )
		-- vCell2.state = "enabled"
		vCell2.groupId = 2
	end

	-- horizontal bottom-left — top-right
	local hBounds = { 
		{2,1}, {3,1}, {4,1}, {4,2}, {5,3},
		{1,3}, {1,4}, {2,5}, {3,5}, {4,5},
	}
	-- on normal cells
	if x % 2 == 0 then
		vY1, vY2 = y + 1, y
	else
		vY1, vY2 = y, y - 1
	end
	vX1, vX2 = x - 1, x + 1
	-- on bounds cells
	for i = 1, mapWidth do
		if y == hBounds[i][1] and 
			x == hBounds[i][2] then
			vY1 = hBounds[i+5][1]
			vX1 = hBounds[i+5][2]
		end
	end
	for i = 6, mapWidth * 2 do
		if y == hBounds[i][1] and 
			x == hBounds[i][2] then
			vY2 = hBounds[i-5][1]
			vX2 = hBounds[i-5][2]
		end
	end

	-- print( vY1 .. ":" .. vX1 .. "  |  " .. vY2 .. ":" .. vX2 )

	vCell1 = myMap[vY1][vX1]
	if vCell1 then
		vCell1:setFillColor( 0, 0, 255 )
		-- vCell1.state = "enabled"
		vCell1.groupId = 3
	end

	vCell2 = myMap[vY2][vX2]
	if vCell2 then
		vCell2:setFillColor( 0, 0, 255 )
		-- vCell2.state = "enabled"
		vCell2.groupId = 3
	end
end

local function flipCells( groupId )
	for y = 1, mapHeight do
		for x = 1, mapWidth do
			local cell = myMap[y][x]
			if cell then
				local id = cell.groupId or -1
				if id == groupId or cell.state == "selected" then
					print(cell)
					if cell.state == "normal" or 
						cell.state == "selected" then
						cell.state = "hidden"
					else
						cell.state = "normal"
					end
					-- cell.groupId = nil
				end
			end
		end
	end
	clean()
end


----- TOUCH EVENT -----
local function touchEvent( event )
	local event = event
	local target = event.target

	if event.phase == "began" then

		print( target.id )

		if target.state == "normal" and selectedItem == nil then
			target.state = "selected"
			selectedItem = target
			colorize( target.xPos, target.yPos )
		elseif target.groupId ~= nil then
			flipCells( target.groupId )
		else
			clean()
		end
	end
end


----- CREATE MAP -----
local yOffset = 55
local xOffset = 49

for y = 1, mapHeight do
	myMap[y] = {}
	for x = 1, mapWidth do
		-- skip unused cells
		if  ( x == 1 and y == 1 ) or
			( x == 5 and y == 1 ) or
			( x == 1 and y == 5 ) or
			( x == 2 and y == 5 ) or
			( x == 4 and y == 5 ) or
			( x == 5 and y == 5 ) then
		else
			local cell = display.newImageRect( eyesGroup, "assets/images/eye.png", 49, 49 )
			cell:addEventListener( "touch", touchEvent )
			cell.isOpened = true
			cell.id = y .. ":" .. x
			cell.xPos, cell.yPos = x, y
			cell.state = "normal"

			if x % 2 == 0 then
				cell.x = x * xOffset
				cell.y = y * yOffset
			else
				cell.x = x * xOffset
				cell.y = y * yOffset - yOffset * 0.5
			end

			myMap[y][x] = cell
		end
	end
end






